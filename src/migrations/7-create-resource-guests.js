'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Resource_guests', {
      UserId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      ResourceId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Resources',
          key: 'id'
        }
      },
      invitedBy: {
        type: Sequelize.INTEGER
      },
      invitedAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Resource_guests');
  }
};
