import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceImageDB {
    
    constructor() {};

    async search(query) {
        return await db.Resource_images.findAll(query)
    }

    async get(query) {
        return await db.Resource_images.findOne(query);
    }

    async post(query) {
        return await db.Resource_images.create(query);
    }

    async put(query) {
        return await db.Resource_images.update(query);
    }

    async destroy(query) {
        return await db.Resource_images.destroy(query);
    }
}
