import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceLikeDB {
    
    constructor() {};

    async search(query) {
        return await db.Resource_likes.findAll(query)
    }

    async get(query) {
        return await db.Resource_likes.findOne(query);
    }

    async post(query) {
        return await db.Resource_likes.create(query);
    }

    async put(query) {
        return await db.Resource_likes.update(query);
    }

    async destroy(query) {
        return await db.Resource_likes.destroy(query);
    }
}
