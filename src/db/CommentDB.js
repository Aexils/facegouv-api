import db from '../models';
import {Op, where} from 'sequelize';

export default class CommentDB {
    
    constructor() {};
    
    async search(query) {
        return await db.Comments.findAll(query)
    }

    async get(query) {
        return await db.Comments.findOne(query);
    }

    async post(query) {
        return await db.Comments.create(query);
    }

    async put(query) {
        return await db.Comments.update(query);
    }
}
