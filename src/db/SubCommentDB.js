import db from '../models';
import {Op, where} from 'sequelize';

export default class SubCommentDB {
    
    constructor() {};

    async search(query) {
        return await db.Sub_comments.findAll(query)
    }

    async get(query) {
        return await db.Sub_comments.findOne(query);
    }

    async post(query) {
        return await db.Sub_comments.create(query);
    }

    async put(query) {
        return await db.Sub_comments.update(query);
    }

    async destroy(query) {
        return await db.Sub_comments.destroy(query);
    }
}
