import db from '../models';
import {Op, where} from 'sequelize';

export default class FavoriteDB {
    
    constructor() {};

    async search(query) {
        return await db.Favorites.findAll(query)
    }

    async get(query) {
        return await db.Favorites.findOne(query);
    }

    async post(query) {
        return await db.Favorites.create(query);
    }

    async put(query) {
        return await db.Favorites.update(query);
    }

    async destroy(query) {
        return await db.Favorites.destroy(query);
    }
}
