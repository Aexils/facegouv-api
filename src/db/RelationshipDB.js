import db from '../models';
import {Op, where} from 'sequelize';

export default class RelationshipDB {
    
    constructor() {};

    async search(query) {
        return await db.Conv_messages.findAll(query)
    }

    async get(query) {
        return await db.Conv_messages.findOne(query);
    }

    async post(query) {
        return await db.Conv_messages.create(query);
    }

    async put(query) {
        return await db.Conv_messages.update(query);
    }

    async destroy(query) {
        return await db.Conv_messages.destroy(query);
    }
}
