import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceStateDB {
    
    constructor() {};

    async search(query) {
        return await db.Resource_state.findAll(query)
    }

    async get(query) {
        return await db.Resource_state.findOne(query);
    }

    async post(query) {
        return await db.Resource_state.create(query);
    }

    async put(query) {
        return await db.Resource_state.update(query);
    }

    async destroy(query) {
        return await db.Resource_state.destroy(query);
    }
}
