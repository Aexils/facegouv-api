import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceDB {

    constructor() {};

    async search (query) {
        try {
            const select = await db.Resources.findAll(query);
            console.log(select)
            return select;
        } catch (error) {
            console.log(error)
        }
    }

    async get(query) {
        return await new db.Resources.findOne(query);
    }

    async post(query) {
        return await new db.Resources.create(query);
    }

    async put(query) {
        return await db.Resources.update(query);
    }

    async destroy(query) {
        return await db.Resources.destroy(query);
    }
}