import db from '../models';

export default class UserDB {
    
    constructor() {};

    async search(query) {
        return await db.User.findAll(query)
    }

    async get(query) {
        return await db.User.findOne(query);
    }

    async post(query) {
        return await db.User.create(query);
    }

    async put(query) {
        return await db.User.update(query);
    }
}