import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceShareDB {
    
    constructor() {};

    async search(query) {
        return await db.Resource_shares.findAll(query)
    }

    async get(query) {
        return await db.Resource_shares.findOne(query);
    }

    async post(query) {
        return await db.Resource_shares.create(query);
    }

    async put(query) {
        return await db.Resource_shares.update(query);
    }

    async destroy(query) {
        return await db.Resource_shares.destroy(query);
    }
}
