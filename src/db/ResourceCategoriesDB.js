import db from '../models';
import {Op, where} from 'sequelize';

export default class ResourceCategoriesDB {
    
    constructor() {};

    async search(query) {
        return await db.Resource_categories.findAll(query)
    }

    async get(query) {
        return await db.Resource_categories.findOne(query);
    }

    async post(query) {
        return await db.Resource_categories.create(query);
    }

    async put(query) {
        return await db.Resource_categories.update(query);
    }

    async destroy(query) {
        return await db.Resource_categories.destroy(query);
    }
}
