import db from '../models';
import {Op, where} from 'sequelize';

export default class ConversationDB {
    
    constructor() {};
    
    async search(query) {
        return await db.Conversations.findAll(query)
    }

    async get(query) {
        return await db.Conversations.findOne(query);
    }

    async post(query) {
        return await db.Conversations.create(query);
    }

    async put(query) {
        return await db.Conversations.update(query);
    }

    async destroy(query) {
        return await db.Conversations.destroy(query);
    }
}
