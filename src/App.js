import express from 'express';
import cors from 'cors';
import { urlencoded, json } from 'body-parser';
import router from './routes/routes.js';
import routerAdmin from './routes/routesAdmin.js';
import { Server } from 'socket.io';
import { socketRouter } from './routes/socketRoutes.js';
import { createServer } from 'http';

const app = express();

const http = createServer(app);

const io = new Server(http, { 
    cors: {
        origin: ['https://aexils.fr', 'https://api.aexils.fr', 'http://localhost:4200', 'http://31.32.16.60/'],
        methods: ["GET", "POST"],
        transports: ['websocket', 'polling'],
        credentials: true
    },
    path: '/socket',
    allowEIO3: true
});

const whitelist = ['https://aexils.fr', 'https://api.aexils.fr', 'http://localhost:4200', 'http://31.32.16.60/', 'https://mail.google.com'];
    const corsOptions = {
        origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};

// Activer le CORS pour toutes les routes
app.use(cors());

// Activation du moteur de template
app.set('view engine', 'ejs');

//Configuration de Body Parser
app.use(urlencoded({extended: true}));
app.use(json({limit: '50mb'}));

app.use(express.static('./public'));

http.listen(3000, () => {
    console.log(`${process.env.NODE_ENV} mode`);
    console.log("Server started and listening on port 3000");
});

io.on('connection', function(socket) {
    console.log("User connected");
    socketRouter(io, socket);
})

// router
app.use(router);
app.use(routerAdmin);

module.exports = express();
