import CommentDB from "../db/CommentDB";
import UserDB from "../db/UserDb";
import SubCommentDB from "../db/SubCommentDB";
export default class Comment {
//region Comments

  async AddComment(req, res) {

    const userId = req.body.userId;
    const resourceId = req.body.resourceId;
    const content = req.body.content;

    // Verifie si l'utilisateur existe
    const userFound = await new UserDB().get({where: {id: userId}});

    if (!userFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
    if (userFound.UserStateId < 3) throw res.status(403).send({'erreur': 'Droits de l\'utilisateur insuffisants'});

    const newComment = await new CommentDB().post({
      UserId: userId,
      ResourceId: resourceId,
      content: content,
      createdAt: new Date(),
      updatedAt: new Date()
    });

    if (!newComment) throw res.status(500).send({'erreur': 'Ne peut pas insérer le commentaire'});
    return res.status(201).json({'Message': 'Commentaire créé avec succès !'});
  }

  async GetCommentById(req, res) {
    try {

      const comment = await new CommentDB().get({
        where: {id: req.params.commentId},
        include: [db.User, db.Sub_comments, {model: db.Sub_comments, include: db.User}]
      });
      if (!comment) throw res.status(404).send({'erreur': 'Le commentaire n\'existe pas'});

      return res.status(200).json(comment);
    } catch (error) {
      console.log(error)
    }
  }

  async GetAllCommentsByResourceId(req, res) {

    const comment = await new CommentDB().search({
      where: {ResourceId: req.params.resourceId},
      include: [db.User, db.Sub_comments, {model: db.Sub_comments, include: db.User}]
    })
    if (!comment) throw res.status(500).send({'erreur': 'Ressource non trouvée.'});

    return res.status(200).send(comment);
  }

  // Fonction pour mettre à jour un commentaire
  async UpdateComment(req, res) {
    try {

      // Champs étrangers obligatoires
      const userId = req.body.userId;
      const resourceId = req.params.resourceId;
      const commentId = req.params.commentId;

      //  Champs statiques
      const content = req.body.content ?? null;

      // Vérification des données obligatoires
      if (!userId || !commentId || !resourceId) throw res.status(403).send({'erreur': 'Paramètres manquants'});

      // Vérification que le user existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

      // Vérification que le commentaire existe
      const commentFound = await new CommentDB().get({where: {id: commentId}});
      if (!commentFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si le commentaire existe'});

      // Vérification que le user à le droit d'éditer le commentaire
      if (userId !== commentFound?.UserId || userFound.UserStateId < 4)
        throw res.status(403).send({'erreur': 'Droits insuffisants pour éditer le commentaire'});

      // Créer l'objet de mise à jour en fonction des données renseignées

      if (content)
        commentFound.content = content;

      commentFound.updatedAt = Date.now();

      // Met à jour l'objet
      const updated = await new CommentDB().put(commentFound.dataValues, {where: {id: commentId}});
      if (!updated) throw res.status(500).send({'erreur': 'Erreur lors de l\'édition du commentaire'});

      return res.status(200).send({'Message': 'Commentaire édité avec succès !'});
    } catch (err) {
      console.log(err)
    }
  }

  // Fonction pour supprimer un commentaire
  async DeleteComment(req, res) {
    const commentId = req.params.id;

    const deleted = await db.Comments.destroy({where: {id: commentId}});
    if (!deleted) throw res.status(500).send({'erreur': 'Erreur lors de la suppression du commentaire'});

    return res.status(200).send({'Message': 'Commentaire supprimé avec succès !'});
  }

//endregion
//region SubComments
  async AddSubComment(req, res) {

    const userId = req.body.userId;
    const commentId = req.body.commentId;
    const content = req.body.content;

    // Verifie si l'utilisateur existe
    const userFound = await new UserDB().get({where: {id: userId}});

    if (!userFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
    if (userFound.UserStateId < 3) throw res.status(403).send({'erreur': 'Droits de l\'utilisateur insuffisants'});

    const newSubComment = await new SubCommentDB().post({
      UserId: userId,
      CommentId: commentId,
      content: content,
      createdAt: new Date(),
      updatedAt: new Date()
    });

    if (!newSubComment) throw res.status(500).send({'erreur': 'Ne peut pas insérer la réponse'});
    return res.status(201).json({'Message': 'Réponse créé avec succès !'});

  }

  async GetSubCommentById(req, res) {

    const subComment = await new SubCommentDB().get({
      where: {id: req.params.subCommentId},
      include: [db.User]
    })
    if (!subComment) throw res.status(404).send({'erreur': 'Le commentaire n\'existe pas'});

    return res.status(200).json(subComment);
  }

  async GetAllSubCommentsByCommentId(req, res) {
    try {

      const subComments = await new SubCommentDB().search({
        where: {CommentId: req.params.commentId},
        include: [db.User]
      })
      if (!subComments) throw res.status(500).send({'erreur': 'Ressource non trouvée.'});

      return res.status(200).send(subComments);
    } catch (error) {
      console.log(error)
    }
  }

  async UpdateSubComment(req, res) {
    try {

      // Champs étrangers obligatoires
      const userId = req.body.userId;
      const commentId = req.params.commentId;
      const subCommentId = req.params.subCommentId;

      //  Champs statiques
      const content = req.body.content ?? null;

      // Vérification des données obligatoires
      if (!userId || !commentId || !subCommentId) throw res.status(403).send({'erreur': 'Paramètres manquants'});

      // Vérification que le user existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

      // Vérification que le commentaire existe
      const commentFound = await new CommentDB().get({where: {id: commentId}});
      if (!commentFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si le commentaire existe'});

      // Vérification que la réponse existe
      const subCommentFound = await new SubCommentDB().get({where: {id: subCommentId}});
      if (!subCommentFound) throw res.status(500).send({'erreur': 'Ne peut pas vérifier si la réponse existe'});

      // Vérification que le user à le droit d'éditer le commentaire
      if (userId !== commentFound?.UserId || userFound.UserStateId < 4)
        throw res.status(403).send({'erreur': 'Droits insuffisants pour éditer le commentaire'});

      // Créer l'objet de mise à jour en fonction des données renseignées

      if (content)
        subCommentFound.content = content;

      subCommentFound.updatedAt = Date.now();

      // Met à jour l'objet
      const updated = await new SubCommentDB().put(subCommentFound.dataValues, {where: {id: subCommentId}});
      if (!updated) throw res.status(500).send({'erreur': 'Erreur lors de l\'édition de la réponse'});

      return res.status(200).send({'Message': 'Réponse édité avec succès !'});
    } catch (err) {
      console.log(err)
    }
  }
  async DeleteSubComment(req, res) {
    const subCommentId = req.params.subCommentId;

    const deleted = await new SubCommentDB().destroy({where: {id: subCommentId}});
    if (!deleted) throw res.status(500).send({'erreur': 'Erreur lors de la suppression de la réponse'});

    return res.status(200).send({'Message': 'Réponse supprimée avec succès !'});
  }
  //endregion

}
