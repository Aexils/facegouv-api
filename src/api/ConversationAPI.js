import {Op, where} from 'sequelize';
import UserDB from '../db/UserDb';
import ConversationDB from '../db/ConversationDB';
import ConvMessageDB from '../db/ConvMessageDB';
export default class Conversation {

  // SOCKET CALLS
  async CreateConversation(io, socket, sessionMap, data) {
    const adminId = data.adminId;
    const usersId = data.usersId;
    const name = data.name;
    try {
      // Verifie si l'utilisateur existe
      const adminFound = await new UserDB().get({where: {id: adminId}});
      if (!adminFound) {
        throw new Error('Ne peut pas vérifier si l\'administrateur existe');
      }
      const usersFound = await new UserDB().search({
        where: {
          id: {
            [Op.in]: usersId
          }
        }
      });
      if (usersFound.length !== usersId.length) {
        throw new Error('Des utilisateurs n\'existent pas');
      }
      if (!name) {
        throw new Error('Le nom de la conversation est obligatoire');
      }
      const newConversation = await new ConversationDB().post({
        AdminId: adminId,
        name,
        UsersIds: {usersId: usersId},
        createdAt: new Date(),
        updatedAt: new Date()
      });
      if (!newConversation) {
        throw new Error('Ne peut pas créer la conversation');
      }
      let sockets = sessionMap.filter((object) => usersId.includes(object.userId));
      if (sockets.length > 0) {
        sockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("conversationCreated", {message: "Conversation créée"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }


  // SOCKET CALLS
  async RenameConv(io, socket, sessionMap, data) {
    const userId = data.userId;
    const name = data.name;
    const conversationId = data.conversationId;
    try {
      // Verifie si l'utilisateur existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      const conversationFound = await new ConversationDB().get({
        where: {
          id: conversationId
        }
      });
      if (!conversationFound) {
        throw new Error('La conversation n\'existe pas');
      }
      if (!name) {
        throw new Error('Le nom de la conversation est obligatoire');
      }
      if (conversationFound.dataValues.AdminId !== userId && userFound.dataValues.UserStateId < 5) {
        throw new Error('Permission insuffisante pour changer le nom de la conversation');
      }
      const newConversation = await new ConversationDB().put({
        name,
      }, {
        where: {id: conversationId}
      });
      if (!newConversation) {
        throw new Error('Ne peut pas créer la conversation');
      }

      const adminSockets = sessionMap.filter((object) => userId === object.userId);
      const sockets = sessionMap.filter((object) => (userId !== object.userId && JSON.parse(conversationFound.dataValues.UsersIds)).usersId.includes(object.userId));

      if (adminSockets.length > 0) {
        adminSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("conversationRenamed", {message: "La conversation a été renommée"});
        });
      }
      if (sockets.length > 0) {
        sockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("conversationRenamed", {message: "Une conversation a été renommée"});
        });
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Add message to conversation
  async AddMessage(io, socket, sessionMap, data) {
    const userId = data.userId;
    const conversationId = data.conversationId;
    const message = data.message;

    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      if (!message) {
        throw new Error('Message vide');
      }
      if (!(JSON.parse(foundConversation.dataValues.UsersIds)).usersId.includes(userId)) {
        throw new Error('L\'utilisateur n\'est pas dans la conversation');
      }
      const newConversationMessage = await new ConvMessageDB().post({
        ConversationId: conversationId,
        UserId: userId,
        Message: message,
        SeenByIds: {SeenByIds: [userId]},
        createdAt: new Date(),
        updatedAt: new Date()
      });

      if (!newConversationMessage) {
        throw new Error('Ne peut pas ajouter le message');
      }
      let sockets = sessionMap.filter((object) => (JSON.parse(foundConversation.dataValues.UsersIds)).usersId.includes(object.userId));
      if (sockets.length > 0) {
        sockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("messageAdded", {message: "Message ajouté"});
        })
      }
    } catch (error) {
      socket.emit('error', error);
    }
  }


  // remove message from conversation
  async RemoveMessage(io, socket, sessionMap, data) {
    const userId = data.userId;
    const messageId = data.messageId;
    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundMessage = await new ConvMessageDB().get({where: {id: messageId}});
      if (!foundMessage) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      if (foundMessage.dataValues.UserId !== userId && foundUser.dataValues.UserStateId < 5) {
        throw new Error('Permission refusée pour la suppression');
      }
      const messagesDeleted = await new ConvMessageDB().put({hasBeenDeleted: true}, {where: {id: messageId}});
      if (!messagesDeleted) {
        throw new Error('Erreur lors de la suppression du messages dans la conversation');
      }
      let sockets = sessionMap.filter((object) => (JSON.parse(foundConversation.dataValues.UsersIds)).usersId.includes(object.userId));
      if (sockets.length > 0) {
        sockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("messageRemoved", {message: "Message supprimé"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  async AddToConversation(io, socket, sessionMap, data) {
    const userId = data.userId;
    const newUserId = data.newUserId;
    const conversationId = data.conversationId;

    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('L\'utilisateur n\'existe pas');
      }

      const foundNewUser = await new UserDB().get({where: {id: newUserId}});
      if (!foundNewUser) {
        throw new Error('L\'utilisateur à ajouter n\'existe pas');
      }

      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }

      let currentUsers = (JSON.parse(foundConversation.dataValues.UsersIds)).usersId;
      if (!currentUsers.includes(newUserId)) {
        currentUsers.push(newUserId);
        const UsersIds = {"usersId": currentUsers};
        const updated = await new ConversationDB().put({
            UsersIds
          },
          {
            where: {
              id: conversationId
            }
          });
        if (!updated) {
          throw new Error('Ne peut pas ajouter l\'utilisateur');
        }

        const adminSockets = sessionMap.filter((object) => userId === object.userId);
        const participantsSockets = sessionMap.filter((object) => userId !== object.userId && newUserId !== object.userId && (JSON.parse(foundConversation.dataValues.UsersIds)).usersId.includes(object.userId));
        const newUserSockets = sessionMap.filter((object) => newUserId === object.userId);

        if (adminSockets.length > 0) {
          adminSockets.forEach((userSocket) => {
            io.to(userSocket.socketId).emit("userAddedToConv", {message: "L'utilisateur a été ajouté à la conversation"});
          })
        }
        if (participantsSockets.length > 0) {
          participantsSockets.forEach((userSocket) => {
            io.to(userSocket.socketId).emit("userAddedToConv", {message: "Un utilisateur a été ajouté à la conversation"});
          })
        }
        if (newUserSockets.length > 0) {
          newUserSockets.forEach((userSocket) => {
            io.to(userSocket.socketId).emit("userAddedToConv", {message: "Vous avez été ajouté à une conversation"});
          })
        }
      } else {
        throw new Error('L\'utilisateur est déjà dans la conversation');
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Read all messages for user
  async ReadMessages(io, socket, sessionMap, data) {
    const userId = data.userId;
    const conversationId = data.conversationId;
    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      const messages = await new ConvMessageDB().search({where: {ConversationId: conversationId}});
      if (!messages) {
        throw new Error('Pas de message dans la conversation');
      }
      let errorOccured = false;
      messages.forEach((message) => {
        let messageId = JSON.parse(message.dataValues.SeenByIds).id;
        let seenBy = JSON.parse(message.dataValues.SeenByIds).SeenByIds;
        if (!seenBy.includes(userId)) {
          seenBy.push(userId);
          const SeenByIds = {"SeenByIds": seenBy};
          const updated = new ConvMessageDB().put({
              SeenByIds
            },
            {
              where: {
                id: messageId
              }
            });
          if (!updated) {
            errorOccured = true;
          }
        }
      })
      if (!errorOccured) {
        let sockets = sessionMap.filter((object) => object.userId === userId);
        if (sockets.length > 0) {
          sockets.forEach((userSocket) => {
            io.to(userSocket.socketId).emit("messagesReaded", {message: 'Les messages ont été lu'});
          })
        }
      } else {
        throw new Error('Erreur lors de la lecture des messages');
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Leave the conversation
  async LeaveConversation(io, socket, sessionMap, data) {
    const userId = data.userId;
    const conversationId = data.conversationId;
    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      // Quitte
      let convUsers = JSON.parse(foundConversation.dataValues.UsersIds).usersId;
      if (!convUsers.includes(userId)) {
        throw new Error('L\'utilisateur n\'est pas dans la conversation');
      }
      convUsers = convUsers.filter((id) => id !== userId);
      const UsersIds = {"usersId": convUsers};
      const removed = await new ConversationDB().put({
          UsersIds
        },
        {
          where: {
            id: conversationId
          }
        });
      if (!removed) {
        throw new Error('Erreur lors du départ de la conversation');
      }

      const userSockets = sessionMap.filter((object) => userId === object.userId);
      const participantsSockets = sessionMap.filter((object) => userId !== object.userId && JSON.parse(foundConversation.dataValues.UsersIds).usersId.includes(object.userId));

      if (userSockets.length > 0) {
        userSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("userLeft", {message: "Vous avez quitté la conversation"});
        })
      }
      if (participantsSockets.length > 0) {
        participantsSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("userLeft", {message: "Un utilisateur a quitté la conversation"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Delete the conversation
  async DeleteConversation(io, socket, sessionMap, data) {
    const userId = data.userId;
    const conversationId = data.conversationId;
    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      if (foundConversation.AdminId !== userId && userFound.UserStateId < 5) {
        throw new Error('Suppression interdite: Droits insuffisants');
      }

      // Supprime
      await new ConvMessageDB().destroy({where: {ConversationId: conversationId}});
      const deleted = await new ConversationDB().destroy({where: {id: conversationId}});
      if (!deleted) {
        throw new Error('Erreur lors de la suppression de la conversation');
      }

      const adminSockets = sessionMap.filter((object) => userId === object.userId);
      const usersSockets = sessionMap.filter((object) => userId !== object.userId && JSON.parse(foundConversation.dataValues.UsersIds).usersId.includes(object.userId));

      if (adminSockets.length > 0) {
        adminSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("conversationDeleted", {message: "La conversation a été supprimée"});
        })
      }
      if (usersSockets.length > 0) {
        usersSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("conversationDeleted", {message: "La conversation a été supprimée"});
        })
      }
    } catch (error) {
      socket.emit('error', {message: error.message});
    }
  }

  // Kick a user from conversation
  async KickFromConversation(io, socket, sessionMap, data) {
    const userId = data.userId;
    const userToKick = data.userToKick;
    const conversationId = data.conversationId;
    try {
      // Verifie si l'utilisateur existe
      const foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur existe');
      }
      // Verifie si l'utilisateur existe
      const foundKick = await new UserDB().get({where: {id: userToKick}});
      if (!foundKick) {
        throw new Error('Ne peut pas vérifier si l\'utilisateur à exclure existe');
      }
      // Verifie si l'utilisateur existe
      const foundConversation = await new ConversationDB().get({where: {id: conversationId}});
      if (!foundConversation) {
        throw new Error('Ne peut pas vérifier si la conversation existe');
      }
      // Quitte
      let users = (JSON.parse(foundConversation.dataValues.UsersIds)).usersId;
      if (!users.includes(userToKick)) {
        throw new Error('L\'utilisateur n\'est plus dans la conversation');
      }
      users = users.filter((id) => id !== userToKick);
      const UsersIds = {"usersId": users};
      const removed = await new ConversationDB().put({
          UsersIds
        },
        {
          where: {
            id: conversationId
          }
        }
      );
      if (!removed) {
        throw new Error('Erreur lors du ban de la conversation');
      }

      const kicksockets = sessionMap.filter((object) => object.userId === userToKick);
      if (kicksockets.length > 0) {
        kicksockets.forEach((userSocket) => {
          socket.to(userSocket.socketId).emit("kickedFromConversation", {message: "Vous avez été banni de la conversation"});
        })
      }

      const adminSockets = sessionMap.filter((object) => object.userId === userId);
      if (adminSockets.length > 0) {
        adminSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("userKickedFromConv", {message: "L'utilisateur a été banni"});
        })
      }

      const participantsSockets = sessionMap.filter((object) => (object.userId === userId && object.userId === userToKick && JSON.parse(foundConversation.dataValues.UsersIds)).usersId.includes(object.userId) && object.userId !== userToKick);
      if (participantsSockets.length > 0) {
        participantsSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("userKickedFromConv", {message: "Un utilisateur a été banni"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // HTTP ROUTE
  async GetConversations(req, res) {
    const userId = parseInt(req.params.userId);

    let foundUser, foundConversations, messages;
    // Verifie si l'utilisateur existe
    try {
      foundUser = await new UserDB().get({where: {id: userId}});
      if (!foundUser) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
    } catch (error) {
      return res.status(500).send({'erreur': error.message});
    }

    let conversationsList = [];
    // Verifie si l'utilisateur existe
    try {
      foundConversations = await new ConversationDB().search();
      if (!foundConversations) return res.status(200).send(conversationsList);
    } catch (error) {
      return res.status(500).send({'erreur': error.message});
    }

    foundConversations.forEach((conversation) => {
      let conversationUsers = (JSON.parse(conversation.dataValues.UsersIds)).usersId;
      if (conversationUsers.includes(userId) || conversation.dataValues.AdminId === userId) {
        conversationsList.push(conversation.dataValues);
      }
    });

    const promises = [];

    try {
      conversationsList.forEach((conversation) => {
        promises.push(new ConvMessageDB().search({where: {conversationId: conversation.id}}));
      });
      messages = (await Promise.all(promises));
      if (!messages) return res.status(500).send({'erreur': 'Ne peut pas récupérer les messages de la conversation'});
    } catch (error) {
      return res.status(500).send({'erreur': error.message});
    }

    const mesConvPairs = [];
    messages.forEach((messageList) => {
      if (messageList.length > 0) {
        let messagesFormat = [];
        const conversationId = messageList[0].ConversationId;
        messageList.forEach((mess) => {
          mess.dataValues.SeenByIds = (JSON.parse(mess.dataValues.SeenByIds)).SeenByIds;
          delete mess.dataValues.ConversationId;
          messagesFormat.push(mess.dataValues);
        });
        mesConvPairs.push({conversationId, messages: messagesFormat})
      }
    })

    conversationsList.forEach((conversation) => {
      conversation.UsersIds = (JSON.parse(conversation.UsersIds)).usersId;
      let index = mesConvPairs.findIndex((pair) => pair.conversationId === conversation.id);
      if (index !== -1) {
        conversation.messages = mesConvPairs[index].messages;
      } else {
        conversation.messages = [];
      }
    });

    return res.status(200).json(conversationsList);
  }

  async GetConversation(req, res) {
    try {
      const convId = req.params.convId;

      let conversation;
      // Verifie si l'utilisateur existe

      conversation = await new ConversationDB().get({
        where: {
          id: convId
        },
        include: [
          {model: db.Conv_messages, as: 'messages', include: [db.User]}
        ]
      });
      if (!conversation) return res.status(404).send('Conversation introuvable');

      return res.status(200).json(conversation);
    } catch (error) {
      console.log(error)
    }
  }
}
