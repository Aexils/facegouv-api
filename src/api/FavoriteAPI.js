import db from '../models';
import sequelize from "sequelize";
import UserDB from '../db/UserDb';
import FavoriteDB from '../db/FavoriteDB';
import ResourceDB from '../db/ResourceDB';
export default class FavoriteAPI {

  async GetAllFavorites(req, res) {
    try {
      const favorites = await new FavoriteDB().search(
        {
          where: {
            userId: req.params.userId
          },
          include: [db.Resources,
            {model: db.Resources, include: [db.Resource_images, db.User]}
          ]
        });
      return res.status(200).json(favorites);
    } catch (err) {
      console.log(err)
      return await res.status().send(err);
    }
  }

  async CreateFavorite(req, res) {
    const userId = req.body.userId;
    const resourceId = req.body.resourceId;

    // Verifie si l'utilisateur existe
    const userFound = await new UserDB().get({where: {id: userId}});
    if (!userFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

    if (userFound.UserStateId < 2 || !userFound.isEnabled) return res.status(403).send({'erreur': 'Droits de l\'utilisateur insuffisants'});

    // Verifie si la rsesource existe
    const resourceFound = await new ResourceDB().get({where: {id: resourceId}});
    if (!resourceFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si la resource existe'});
    const newFavorite = await new FavoriteDB().post({
      UserId: userId,
      ResourceId: resourceId
    });
    if (!newFavorite) return res.status(500).send({'erreur': 'Ne peut pas ajouter aux favoris'});
    return res.status(201).json({'Message': 'Ressource ajouté aux favoris avec succès !'});
  }

  async RemoveFavorite(req, res) {
    const favoriteId = req.params.id;

    const deleted = await new FavoriteDB().destroy({where: {id: favoriteId}});
    if (!deleted) throw res.status(500).send({'erreur': 'Erreur lors de la suppression de du favoris'});

    return res.status(200).send({'Message': 'Favoris supprimée avec succès !'});
  }
}

