import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import Mailer from '../services/mailer';
import UserDB from '../db/UserDb';

const EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
const PASSWORD_REGEX = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/;

export default class Auth {

    // Méthode pour s'inscrire 
    async Register(req, res) {
        // Récupérer les paramètres
        const firstName = req.body.firstName;
        const lastName = req.body.lastName;
        const age = req.body.age;
        const email = req.body.email
        const password = req.body.password;
        const tel = req.body.tel;
        const postalCode = req.body.postalCode;

        if (firstName == null || lastName == null || age == null || email == null || password == null || tel == null || postalCode == null)
            return res.status(400).send({'erreur': 'Paramètre(s) manquant(s)'});

        if (!EMAIL_REGEX.test(email))
            return res.status(400).send({'erreur': 'L\'email n\'est pas valide'});


        if (!PASSWORD_REGEX.test(password))
            return res.status(400).send({'erreur': 'Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre'});

        const user = await new UserDB().get({where: {email: email}});

        let bcryptedPassword;

        if (!user)
            bcryptedPassword = bcrypt.hashSync(password, 5);
        else
            return res.status(409).send({'erreur': 'L\'adresse email existe déjà'});

        if (!bcryptedPassword)
            return res.status(460).send({'erreur': 'Le hashage du mot de passe a échoué'});

        let newUser = await new UserDB().post({
            UserStateId: 2,
            firstName: firstName,
            lastName: lastName,
            age: age,
            email: email,
            password: bcryptedPassword,
            tel: tel,
            postalCode: postalCode,
            isEnabled: 0,
            profilePicture: 'shiba.jpg'
        });

        if (!newUser) return res.status(500).send({'erreur': 'Ne peut pas inscrire l\'utilisateur'});
        res.render('../views/mail_activation.ejs', {user: newUser}, (err, str) => {
            if (err) return res.status(500).json({'erreur': 'Impossible de récuper le contenu de l\'email'});
            else {
                const mainOptions = {
                    from: '"RESOURCES RELATIONNELLES" contact@resources-relationnelles.fr',
                    to: email,
                    subject: 'Validation de votre compte',
                    html: str
                };
                Mailer.transporter.sendMail(mainOptions);
            }
            return res.status(201).send({'Message': 'Utilisateur crée avec succès !'});
        });
    }

    //Méthode pour activer un compte
    async Activation(req, res) {
        const id = req.body.id;

        await new UserDB().put(
            {isEnabled: 1, UserStateId: 3},
            {where: {id: id}}
        );

        res.render('../views/final_activation.ejs');
    }

    // Méthode pour se connecter
    async Login(req, res) {
        // Récupérer les paramètres
        const email = req.body.email;
        const password = req.body.password;

        if (email == null || password == null)
            return res.status(400).send({'erreur': 'paramètre(s) manquant(s)'});

        // Vérifie l'email de l'utilisateur
        const userFound = await new UserDB().get({where: {email: email}});
        if (!userFound)
            return res.status(404).send({'erreur': 'L\'utilisateur n\'existe pas'});

        // Compare le mot de passe avec celui de la bdd
        const isValidPassword = await bcrypt.compare(password, userFound.password);
        if (!isValidPassword)
            return res.status(403).send({'erreur': 'Mot de passe incorrect'});

        // Si le mot de passe correspond bien
        const user = {
            id: userFound.id,
            userStateId: userFound.UserStateId,
            firstName: userFound.firstName,
            lastName: userFound.lastName,
            age: userFound.age,
            email: userFound.email,
            tel: userFound.tel,
            postalCode: userFound.postalCode,
            isEnabled: userFound.isEnabled,
            profilePicture: userFound.profilePicture
        }

        const token = jwt.sign({user}, 'my_secret_key');

        return res.status(200).json(token);

    }

    //Méthode du mot de passe oublié
    async ForgottenPassword(req, res) {
        // Récupérer les paramètres
        const email = req.body.email;

        if (!EMAIL_REGEX.test(email))
            return res.status(400).send({'erreur': 'L\'email n\'est pas valide'});

        // Vérifie l'email de l'utilisateur
        const userFound = await new UserDB().get({where: {email: email}});

        if (!userFound)
            return res.status(404).send({'erreur': 'L\'utilisateur n\'existe pas'});

        const pwd = Math.random().toString(36).substr(2);
        const pwdHashed = bcrypt.hashSync(pwd, 5);

        await new UserDB().put({password: pwdHashed}, {where: {email: email}});

        res.render('../views/mail_new_password.ejs', {pwd: pwd}, (err, str) => {
            if (err) return res.status(500).json({'erreur': 'Impossible de récuper le contenu de l\'email'});
            else {
                const mainOptions = {
                    from: '"RESOURCES RELATIONNELLES" contact@resources-relationnelles.fr',
                    to: email,
                    subject: 'Votre nouveau mot de passe',
                    html: str
                };
                Mailer.transporter.sendMail(mainOptions);
            }
        });

        return res.status(200).send({"message": "Mot de passe modifié avec succès"});
    }

    //Méthode pour update un utilisateur
    async UpdateAccount(req, res) {
        // Récupérer les paramètres
        const user = JSON.parse(req.body.user);
        let profilePicture;
        if (req.files.image !== undefined) {
           profilePicture  = req.files.image[0]?.filename;
        } else {
            profilePicture = user.profilePicture;
        }

        if (!EMAIL_REGEX.test(user.email))
            return res.status(400).send({'erreur': 'L\'email n\'est pas valide'});

        // Vérifie si l'utilisateur existe
        const userFound = await new UserDB().get({where: {id: user.id}});

        if (!userFound)
            return res.status(404).send({'erreur': 'L\'utilisateur n\'existe pas'});

        let updatedUser = await new UserDB().put(
            {
                firstName: user.firstName,
                lastName: user.lastName,
                age: user.age,
                email: user.email,
                tel: user.tel,
                postalCode: user.postalCode,
                profilePicture: profilePicture
            },
            {where: {id: user.id}}
        )

        if (!updatedUser) return res.status(500).send({'erreur': 'Ne peut pas mettre à jour l\'utilisateur'});

        updatedUser = await new UserDB().get({where: {id: user.id}});

        return res.status(200).send({
            'message': 'Profil mis à jour avec succès !',
            'user': updatedUser
        });
    }

    //Méthode pour update son mot de passe
    async UpdatePassword(req, res) {
        // Récupérer les paramètres
        const id = req.body.id;
        const password = req.body.password;
        const newPassword = req.body.newPassword;

        if (password == null || id == null || newPassword == null)
            return res.status(400).send({'erreur': 'Paramètre(s) manquant(s)'});

        if (!PASSWORD_REGEX.test(newPassword))
            return res.status(400).send({'erreur': 'Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre'});

        // Vérifie si l'utilisateur existe
        const userFound = await new UserDB().get({where: {id: id}});

        if (!userFound)
            return res.status(404).send({'erreur': 'L\'utilisateur n\'existe pas'});

        // Compare le mot de passe avec celui de la bdd
        const isValidPassword = await bcrypt.compare(password, userFound.password);
        if (!isValidPassword)
            return res.status(403).send({'erreur': 'Mot de passe incorrect'});

        let bcryptedPassword = bcrypt.hashSync(newPassword, 5);

        if (!bcryptedPassword)
            return res.status(460).send({'erreur': 'Le hashage du mot de passe a échoué'});

        let user = await new UserDB().put(
            {password: bcryptedPassword},
            {where: {id: id}}
        );

        if (!user) return res.status(500).send({'erreur': 'Ne peut pas mettre à jour l\'utilisateur'});

        return res.status(200).send({'message': 'Mot de passe mis à jour avec succès !'});
    }

    async GetUserById(req, res) {
        const userId = req.params.id;

        const user = await new UserDB().get({where: {id: UserId}});

        if (!user) return res.status(404).send({'error': 'Utilisateur introuvable'})

        return res.status(200).json(user);
    }
}
