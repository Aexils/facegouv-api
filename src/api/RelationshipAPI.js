import db from '../models';
import {Op} from 'sequelize';
import UserDB from '../db/UserDb';
import RelationshipDB from '../db/RelationshipDB';
export default class Relationship {

  async AddFriend(io, socket, sessionMap, data) {
    const senderId = data.senderId;
    const receiverId = data.receiverId;

    try {
      // Verifie si l'utilisateur existe
      const senderFound = await new UserDB().get({where: {id: senderId}});
      if (!senderFound) {
        throw new Error('L\'utilisateur n\'existe pas');
      }
      const receiverFound = await new UserDB().get({where: {id: receiverId}});
      if (!receiverFound) {
        throw new Error('L\'ami a ajouter n\'existe pas');
      }
      const relationExist = await new RelationshipDB().get({
        where: {
          [Op.and]: [
            {
              [Op.or]: {
                SenderId: senderId,
                receiverId: senderId
              },
            },
            {
              [Op.or]: {
                SenderId: receiverId,
                receiverId: receiverId
              }
            }
          ]
        }
      });
      if (relationExist) {
        throw new Error('La relation existe déjà');
      }
      const newRelation = await new RelationshipDB().post({
        SenderId: senderId,
        ReceiverId: receiverId,
        IsConfirmed: false,
        createdAt: new Date(),
        updatedAt: new Date()
      });

      if (!newRelation) {
        throw new Error('Ne peut pas ajouter l\'ami');
      }

      const senderSockets = sessionMap.filter((object) => object.userId === senderId);
      const receiverSockets = sessionMap.filter((object) => object.userId === receiverId);

      if (senderSockets.length > 0) {
        senderSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendAdded", {message: "Demande d'ami envoyée"});
        })
      }

      if (receiverSockets.length > 0) {
        receiverSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendAdded", {message: "Nouvelle demande d'ami"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Remove user from friendList
  async RemoveFriend(io, socket, sessionMap, data) {
    const relationId = data.relationId;
    const userId = data.userId;

    try {
      // Verifie si la relation existe
      const relationFound = await new RelationshipDB().get({where: {id: relationId}});
      if (!relationFound) {
        throw new Error('La relation n\'existe pas');
      }
      // Verifie si l'utilisateur existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) {
        throw new Error('L\'utilisateur n\'existe pas');
      }
      // Verifie si l'utilisateur à le droit de supprimer
      if (relationFound.dataValues.SenderId !== userId && relationFound.dataValues.ReceiverId !== userId && userFound.dataValues.UserStateId < 5) {
        throw new Error("Permission refusée pour la suppression");
      }
      // Supprime
      const deleted = await new RelationshipDB().destroy({where: {id: relationId}});
      if (!deleted) {
        throw new Error('Erreur lors de la suppression de la relation');
      }
      const senderSockets = sessionMap.filter((object) => object.userId === relationFound.dataValues.SenderId);
      const receiverSockets = sessionMap.filter((object) => object.userId === relationFound.dataValues.ReceiverId);
     
      if (senderSockets.length > 0) {
        senderSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendRemoved", {message: "L'ami a été supprimé"});
        })
      }

      if (receiverSockets.length > 0) {
        receiverSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendRemoved", {message: "Un ami vous a supprimé"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }

  // Confirm user to friendList
  async ConfirmFriend(io, socket, sessionMap, data) {
    const userId = data.userId;
    const relationId = data.relationId;

    try {
      // Verifie si la relation existe
      const relationFound = await new RelationshipDB().get({where: {id: relationId}});
      if (!relationFound) {
        throw new Error('La relation n\'exite pas');
      }
      // Verifie si l'utilisateur existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) {
        throw new Error('L\'utilisateur n\'existe pas');
      }
      if (relationFound.dataValues.ReceiverId !== userId && userFound.dataValues.UserStateId < 5) {
        throw new Error('Permission refusée pour la confirmation');
      }

      relationFound.dataValues.isConfirmed = true;
      relationFound.dataValues.updatedAt = Date.now();
      const updated = await new RelationshipDB().put(relationFound.dataValues, {where: {id: relationId}});
      if (!updated) {
        throw new Error('Erreur lors de l\'édition de la relation');
      }

      const senderSockets = sessionMap.filter((object) => object.userId === relationFound.dataValues.senderId);
      const receiverSockets = sessionMap.filter((object) => object.userId === relationFound.dataValues.ReceiverId);

      if (senderSockets.length > 0) {
        senderSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendshipConfirmed", {message: "L'ami a été accepté"});
        })
      }

      if (receiverSockets.length > 0) {
        receiverSockets.forEach((userSocket) => {
          io.to(userSocket.socketId).emit("friendshipConfirmed", {message: "Un ami a accepté votre demande"});
        })
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }

  }

  // HTTP REQUESTS
  // Confirm user to friendList
  async GetRelations(req, res) {
    const userId = parseInt(req.params.userId);
    // Verifie si l'utilisateur existe
    const userFound = await new UserDB().get({where: {id: userId}});
    if (!userFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});
    try {
      const relations = await new RelationshipDB().search({
        where: {
          [Op.or]: [
            {SenderId: userId},
            {ReceiverId: userId}
          ]
        },
        include: [{
          model: db.User,
          as: 'Sender'
        },
          {
            model: db.User,
            as: 'Receiver'
          }]
      });
      if (!relations) throw new Error('Erreur lors de la récupération des relations');
      res.status(200).json(relations);

    } catch ({message}) {
      res.status(500).send({message});
    }
  }

}
