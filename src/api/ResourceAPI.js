import db from '../models';
import sequelize, {Op} from "sequelize";
import UserDB from '../db/UserDb';
import ResourceDB from '../db/ResourceDB';
import ResourceCategoriesDB from '../db/ResourceCategoriesDB';
import ResourceImageDB from '../db/ResourceImageDB';
import ResourceLikeDB from '../db/ResourceLikeDB';
import ResourceShareDB from '../db/ResourceShareDB';
export default class Resource {

  async CreateResource(req, res) {
    const resource = JSON.parse(req.body.resource);
    let image;
    if (req.files.image !== undefined) {
      image  = req.files.image[0]?.filename;
    } else {
      image = 'indisponible.jpg';
    }

    let eventDate = resource.resource.eventDate;
    let eventPlace = resource.resource.eventPlace;

    if (resource.resource.category !== 'Événement') {
      eventDate = null;
      eventPlace = null;
    }

    if (!resource.userId || !resource.resource.category || !resource.resource.title || !resource.resource.description)
      return res.status(400).send({'erreur': 'Paramètre(s) manquant(s)'});

    // Verifie si l'utilisateur existe
    const userFound = await new UserDB().get({where: {id: resource.userId}});
    if (!userFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

    if (userFound.UserStateId < 2 || !userFound.isEnabled) return res.status(403).send({'erreur': 'Droits de l\'utilisateur insuffisants'});

    const typeFound = await ResourceCategoriesDB.get({where: {type: resource.resource.category}});
    if (!typeFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si la catégorie de ressource existe'});

    const newResource = await new new ResourceDB().post({
      UserId: userFound.id,
      ResourceCategoryId: typeFound.id,
      ResourceStateId: 1,
      title: resource.resource.title,
      description: resource.resource.description,
      eventDate: eventDate,
      eventPlace: eventPlace,
      viewCount: 0,
      searchCount: 0,
      likeCount: 0,
      createdAt: new Date(),
      updatedAt: new Date()
    });
    if (!newResource) return res.status(500).send({'erreur': 'Ne peut pas ajouter la ressource'});

    if (image) {
      const newResourceImage = await ResourceImageDB.post({
        ResourceId: newResource.dataValues.id,
        image: image,
        createdAt: new Date(),
        updatedAt: new Date()
      });
      if (!newResourceImage) return res.status(500).send({'erreur': 'Ne peut pas ajouter l\'image de la ressource'});
    }

    return res.status(201).json({'Message': 'Ressource créée avec succès !'});
  }

  async GetResourceById(req, res) {
    try {
      const resource = await new ResourceDB().get(
        {
          where: {id: req.params.id},
          include: [db.Resource_images, db.User, db.Comments, db.Resource_likes,
            {
              model: db.Comments, include: [db.User, db.Sub_comments,
                {model: db.Sub_comments, include: db.User}]
            }]
        });
      return res.status(200).json(resource);
    } catch (err) {
      return await res.status().send(err);
    }

  }

  async GetResourcesByNumberAndDateOrder(req, res) {
    try {
      const resources = await new ResourceDB().search({
        limit: parseInt(req.params.number),
        where: {ResourceStateId: 2},
        order: sequelize.literal('createdAt DESC'),
        include: [db.Resource_images, db.User]
      })
      console.log("on sort")
      if (!resources) return res.status(404).send({'erreur': 'La ressource n\'existe pas'});
      return res.status(200).json(resources);
    } catch (error) {
      return res.status(400).send('NotFound');
    }
    
  }

  async GetResourcesByUserId(req, res) {
    const userId = req.params.userId;
    const resources = await new ResourceDB().search({
      where: {UserId: userId},
      order: sequelize.literal('createdAt DESC'),
      include: [db.Resource_images, db.User]
    })
    if (!resources) return res.status(404).send({'erreur': 'L\'utilisateur n\'existe pas'});

    return res.status(200).json(resources);
  }

  async GetAllResources(req, res) {
    console.log(req)
    const resource = await new ResourceDB().search({where: req.query, include: ['User']})
    if (!resource) return res.status(500).send({'erreur': 'Ne peut pas vérifier si le type de ressource existe'});

    return res.status(200).send(resource);
  }

  async GetAllResourcesNotValided(req, res) {

    const resource = await new ResourceDB().search({where: {ResourceStateId: 1}})
    if (!resource) return res.status(500).send({'erreur': 'Ne peut pas vérifier si le type de ressource existe'});
    return res.status(200).send({data: resource.length});
  }

  async LikeResource(req, res) {
    try {
      const userId = req.body.userId;
      const resourceId = req.body.resourceId;

      // Verifie si l'utilisateur existe
      const userFound = await new UserDB().get({where: {id: userId}});
      if (!userFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

      if (userFound.UserStateId < 2 || !userFound.isEnabled) return res.status(403).send({'erreur': 'Droits de l\'utilisateur insuffisants'});

      // Verifie si la rsesource existe
      const resourceFound = await new ResourceDB().get({where: {id: resourceId}});
      if (!resourceFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si la resource existe'});

      const likeFound = await ResourceLikeDB.get({where: {userId: userId, resourceId: resourceId}})
      if (likeFound) {
        const deleted = await ResourceLikeDB.destroy({where: {id: likeFound.id}});
        if (!deleted) return res.status(500).send({'erreur': 'Erreur lors de la suppression de la resource'});

        return res.status(201).json({'Message': 'La ressource n\'est plus aimée !'});

      } else {

        const newLike = await ResourceLikeDB.post({
          UserId: userId,
          ResourceId: resourceId
        });
        if (!newLike) return res.status(500).send({'erreur': 'Ne peut pas aimer la resource'});

        return res.status(201).json({'Message': 'Ressource aimée avec succès !'});
      }

    } catch (err) {
      console.log(err)
    }
  }

  async IncrementResourceView(req, res) {
    const resourceId = req.body.resourceId;

    // Verifie si la rsesource existe
    const resourceFound = await new ResourceDB().get({where: {id: resourceId}});
    if (!resourceFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si la resource existe'});

    await new ResourceDB().put({viewCount: resourceFound.dataValues.viewCount += 1}, {where: {id: resourceFound.dataValues.id}});
    return res.status(200).json({'Message': 'Compteur de vue de la ressource incrémenté avec succès !'});
  }

  // Fonction pour mettre à jour une ressource
  // TODO à revoir, update by id en paramètre d'url c'est pas top
  // TODO test unitaire à faire
  async UpdateResource(req, res) {

    let updateObject = {};

    // Champs étrangers obligatoires
    const userId = req.body.userId;
    const resourceId = req.params.id;

    // Champs étrangers
    const resourceState = req.body.resourceState ? req.body.resourceState : null;
    const resourceType = req.body.resourceType ? req.body.resourceType : null;

    //  Champs statiques
    const title = req.body.title ? req.body.title : null;
    const description = req.body.description ? req.body.description : null;
    const eventDate = req.body.eventDate ? req.body.eventDate : null;
    const eventPlace = req.body.eventPlace ? req.body.eventPlace : null;

    // Vérification des données obligatoires
    if (!userId || !resourceId) return res.status(403).send({'erreur': 'Paramètres manquants'});

    // Vérification que le user existe
    const userFound = await new UserDB().get({where: {id: userId}});
    if (!userFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'utilisateur existe'});

    // Vérification que la ressource existe
    const resourceFound = await new ResourceDB().get({where: {id: resourceId}});
    if (!resourceFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si la ressource existe'});

    // Vérification que le user à le droit de modifier la ressource
    if (userFound.id !== resourceFound.userId && userFound.UserStateId < 4)
      return res.status(403).send({'erreur': 'Droits insuffisants pour modifier la ressource'});

    //  Vérifie si le type de la ressouce change et s'il existe
    let typeFound = null;
    if (resourceType) {
      typeFound = await ResourceCategoriesDB.get({where: {type: resourceType}});
      if (!typeFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si le type de ressource existe'});
    }

    // Vérifie si le state de la ressouce change et s'il existe
    let stateFound = null;
    if (resourceState) {
      stateFound = await ResourceStateDB.get({where: {state: resourceState}});
      if (!stateFound) return res.status(500).send({'erreur': 'Ne peut pas vérifier si l\'état de la ressource existe'});
    }

    // Créer l'objet de mise à jour en fonction des données renseignées
    if (title) updateObject['title'] = title;
    if (description) updateObject['description'] = description;
    if (eventDate) updateObject['eventDate'] = eventDate;
    if (eventPlace) updateObject['eventPlace'] = eventPlace;
    if (typeFound) updateObject['ResourceCategoryId'] = typeFound.id;
    if (stateFound) updateObject['ResourceStateId'] = stateFound.id;

    updateObject['updatedAt'] = Date.now();

    // Met à jour l'objet
    const updated = await new ResourceDB().put(updateObject, {where: {id: resourceId}});
    if (!updated) return res.status(500).send({'erreur': 'Erreur lors de la mise à jour de la resource'});

    return res.status(200).send({'Message': 'Ressource mise à jour avec succès !'});
  }

  async UpdateResourceStateIdFromAdmin(req, res) {
    // Récupérer les paramètres
    const resourceId = req.body.id;
    let resourceStateId = req.body.resourceStateId;
    const stateAdmin = req.body.stateAdmin;

    if (stateAdmin >= 4) {
      const user = await new ResourceDB().get({where: {id: resourceId}});
      if (!user) return res.status(500).send({'erreur': 'Ressource inconnue'});

      let editResource = await new ResourceDB().put(
        {ResourceStateId: resourceStateId},
        {where: {id: resourceId}}
      );

      if (!editResource) return res.status(500).send({'erreur': 'Ne peut pas mettre à jour la ressource'});

      return res.status(200).send({'message': 'Ressource mis à jour avec succès !'});

    } else {
      return res.status(403).send({'erreur': 'Vous n\'avez pas les droits'});
    }
  }

  // Fonction pour supprimer une ressource
  // TODO à revoir, delete by id en paramètre d'url c'est pas top
  // TODO test unitaire à faire
  async DeleteResource(req, res) {
    const resourceId = req.params.id;

    const deleted = await new ResourceDB().destroy({where: {id: resourceId}});
    if (!deleted) return res.status(500).send({'erreur': 'Erreur lors de la suppression de la resource'});

    return res.status(200).send({'Message': 'Ressource supprimée avec succès !'});
  }

  async GetResourcesShared(req, res) {
    const userId = req.params.userId;

    const userFound = await new UserDB().get({where: {id: userId}});
    if (!userFound) return res.status(500).send({'erreur': 'L\'utilisateur n\'existe pas'});

    const resourcesShared = await ResourceShareDB.search({
      where: {
        [Op.or]: [
          {SenderId: userId},
          {ReceiverId: userId}
        ]
      },
      include: [{
        model: db.User,
        as: 'Sender'
      },
        {
          model: db.User,
          as: 'Receiver'
        },
        {
          model: db.Resources, include: [{
            model: db.User
          }]
        },
      ]
    })

    if (!resourcesShared) return res.status(500).send('Erreur lors de la récupération des ressources partagées');
    res.status(200).json(resourcesShared);
  }

  // Share resource to selected friends
  async ShareResourceToFriends(io, socket, sessionsMap, data) {
    const senderId = data.senderId;
    const resourceId = data.resourceId;
    const receiversId = data.receiversId;

    const senderFound = await new UserDB().get({where: {id: senderId}});
    if (!senderFound) {
      throw new Error(`L\'utilisateur ${id} n\'existe pas`);
    }

    try {
      for (const receiverId of receiversId) {
        const receiverFound = await new UserDB().get({where: {id: receiverId}});
        if (!receiverFound) {
          throw new Error(`L\'utilisateur ${id} n\'existe pas`);
        }
        const resourceSharedFound = await ResourceShareDB.get({
          where: {
            [Op.and]: [
              {SenderId: senderId},
              {ReceiverId: receiverId}
            ]
          }
        })

        if (resourceSharedFound) throw new Error(`Cette ressource a déjà été partagée avec ${receiverFound.firstName}`);

        const newResourceShare = await ResourceShareDB.post({
          SenderId: senderId,
          ReceiverId: receiverId,
          ResourceId: resourceId,
          isViewed: false,
          createdAt: new Date(),
          updatedAt: new Date()
        });
        if (!newResourceShare) {
          throw new Error('Ne peut pas partager la ressource');
        }
        let sockets = sessionsMap.filter((object) => object.userId === senderId || object.userId === receiverId);

        if (sockets.length > 0) {
          sockets.forEach((userSocket) => {
            io.to(userSocket.socketId).emit("resourceShared", {message: "Ressource partagée"});
          })
        }
      }
    } catch ({message}) {
      socket.emit('error', {message});
    }
  }
}
