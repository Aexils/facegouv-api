import Mailer from "../services/mailer";
import {Op} from "sequelize";
import UserDB from '../db/UserDb';

export default class Admin {
    async GetAllUsers(req, res) {
        const users = await new UserDB().search()
        if (!users) return res.status(500).send({'erreur': 'Aucun utilisateurs à afficher'});
        return res.status(200).send(users);
    }

    async GetAllUsersExceptAdmin(req, res) {
        const users = await new UserDB().search({where: { UserStateId: {[Op.or]: [1, 2, 3, 4]}}})
        if (!users) return res.status(500).send({'erreur': 'Aucun utilisateurs à afficher'});
        return res.status(200).send(users);
    }

    async EditUserState(req, res) {
        // Récupérer les paramètres
        const userId = req.body.id;
        let userStateId = req.body.userStateId;
        const stateAdmin = req.body.stateAdmin;

        if (stateAdmin >= 5) {
            const user = await new UserDB().get({where: {id: userId}});
            if (!user) return res.status(500).send({'erreur': 'Utilisateur inconnu'});

            let editUser = await db.User.update(
                {UserStateId: userStateId},
                {where: {id: userId}}
            );

            if (!editUser) return res.status(500).send({'erreur': 'Ne peut pas mettre à jour l\'utilisateur'});

            switch (userStateId) {
                case '1':
                    userStateId = 'Banni';
                    break;
                case '2':
                    userStateId = 'Visiteur';
                    break;
                case '4':
                    userStateId = 'Modérateur';
                    break;
                case '5':
                    userStateId = 'Admin';
                    break;
                case '6':
                    userStateId = 'Super-Admin';
                    break;
                default:
                    userStateId = 'Utilisateur';
            }

            res.render('../views/mail_user_state_edited.ejs', {firstname: user.dataValues.firstName, userStateId: userStateId}, (err, str) => {
                if (err) return res.status(500).json({'erreur': 'Impossible de récuper le contenu de l\'email'})
                else {
                    const mainOptions = {
                        from: '"RESOURCES RELATIONNELLES" contact@resources-relationnelles.fr',
                        to: user.dataValues.email,
                        subject: 'Information concernant votre compte utilisateur',
                        html: str
                    };
                    Mailer.transporter.sendMail(mainOptions);
                }
            });

            return res.status(200).send({'message': 'Utilisateur mis à jour avec succès !'});

        } else {
            return res.status(403).send({'erreur': 'Vous n\'avez pas les droits'});
        }
    }
}
