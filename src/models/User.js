'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasMany(models.Resources);
      models.User.hasMany(models.User_resource_states);
      models.User.hasMany(models.Resource_guests);
      models.User.hasMany(models.Conv_messages);
      models.User.hasMany(models.Favorites);
      models.User.hasMany(models.Resource_likes);
      models.User.hasMany(models.Conversations, {foreignKey: 'AdminId'});
      models.User.hasMany(models.Relationships, {foreignKey: 'SenderId'});
      models.User.hasMany(models.Relationships, {foreignKey: 'ReceiverId'});
      models.User.hasMany(models.Comments);
      models.User.hasMany(models.Sub_comments);
      models.User.belongsTo(models.User_states, {
        foreignKey: {
          allowNull: false
        }
      })
    }
  }

  User.init({
    UserStateId: DataTypes.INTEGER,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    age: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    tel: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    isEnabled: DataTypes.BOOLEAN,
    profilePicture: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
