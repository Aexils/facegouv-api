'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Favorites extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Favorites.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Favorites.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      });
    }
  }
  Favorites.init({
    UserId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Favorites',
  });
  return Favorites;
}
;
