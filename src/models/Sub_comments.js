'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sub_comments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Sub_comments.belongsTo(models.Comments, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Sub_comments.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
    }
  }
  Sub_comments.init({
    CommentId: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER,
    content: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Sub_comments',
  });
  return Sub_comments;
};
