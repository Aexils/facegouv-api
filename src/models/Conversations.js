'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Conversations extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Conversations.hasMany(models.Conv_messages, {as: 'messages'});
      models.Conversations.belongsTo(models.User, {
        foreignKey: 'AdminId',
        targetKey: 'id',
        allowNull: false
      });
    }
  };
  Conversations.init({
    AdminId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    UsersIds: DataTypes.JSON
  }, {
    sequelize,
    modelName: 'Conversations',
  });
  return Conversations;
};
