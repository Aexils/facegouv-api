'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Conv_messages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Conv_messages.belongsTo(models.Conversations, {
        foreignKey: {
          allowNull: false
        }
      })
      models.Conv_messages.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
    }
  };
  Conv_messages.init({
    ConversationId: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER,
    Message: DataTypes.STRING,
    SeenByIds: DataTypes.JSON,
    hasBeenDeleted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Conv_messages',
  });
  return Conv_messages;
};
