'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Comments.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Comments.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Comments.hasMany(models.Sub_comments);
    }
  }

  Comments.init({
    UserId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER,
    content: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Comments',
  });
  return Comments;
};
