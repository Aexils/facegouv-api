'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resource_likes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Resource_likes.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Resource_likes.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      });
    }
  }

  Resource_likes.init({
    UserId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Resource_likes',
  });
  return Resource_likes;
};
