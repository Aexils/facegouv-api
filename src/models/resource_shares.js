'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resource_shares extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Resource_shares.belongsTo(models.User, {
        as: 'Sender',
        foreignKey: {
          name: 'SenderId',
          allowNull: false
        }
      });
      models.Resource_shares.belongsTo(models.User, {
        as: 'Receiver',
        foreignKey: {
          name: 'ReceiverId',
          allowNull: false
        }
      });
      models.Resource_shares.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      });
    }
  }
  Resource_shares.init({
    SenderId: DataTypes.INTEGER,
    ReceiverId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER,
    isViewed: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Resource_shares',
  });
  return Resource_shares;
};
