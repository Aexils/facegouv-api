'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resource_guests extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Resource_guests.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Resource_guests.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      })
    }
  };
  Resource_guests.init({
    UserId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER,
    invitedBy: DataTypes.INTEGER,
    invitedAt: DataTypes.DATE,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Resource_guests',
  });
  return Resource_guests;
};
