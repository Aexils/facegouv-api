'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Relationships extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Relationships.belongsTo(models.User, {
        as: 'Sender',
        foreignKey: {
          name: 'SenderId',
          allowNull: false
        }
      });
      models.Relationships.belongsTo(models.User, {
        as: 'Receiver',
        foreignKey: {
          name: 'ReceiverId',
          allowNull: false
        }
      });
    }
  }
  Relationships.init({
    SenderId: DataTypes.INTEGER,
    ReceiverId: DataTypes.INTEGER,
    isConfirmed: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Relationships',
  });
  return Relationships;
};
