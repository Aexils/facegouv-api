'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_resource_states extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User_resource_states.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.User_resource_states.belongsTo(models.Resources, {
        foreignKey: {
          allowNull: false
        }
      })
    }
  };
  User_resource_states.init({
    UserId: DataTypes.INTEGER,
    ResourceId: DataTypes.INTEGER,
    saved: DataTypes.BOOLEAN,
    favorite: DataTypes.BOOLEAN,
    exploited: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User_resource_states',
  });
  return User_resource_states;
};
