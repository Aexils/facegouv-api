'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resources extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Resources.hasMany(models.User_resource_states);
      models.Resources.hasMany(models.Resource_guests);
      models.Resources.hasMany(models.Resource_images);
      models.Resources.hasMany(models.Favorites);
      models.Resources.hasMany(models.Resource_likes);
      models.Resources.belongsTo(models.Resource_categories, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Resources.belongsTo(models.Resource_state, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Resources.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      })
      models.Resources.hasMany(models.Comments);
    }
  }
  Resources.init({
    UserId: DataTypes.INTEGER,
    ResourceCategoryId: DataTypes.INTEGER,
    ResourceStateId: DataTypes.INTEGER,
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    eventDate: DataTypes.DATE,
    eventPlace: DataTypes.STRING,
    viewCount: DataTypes.INTEGER,
    searchCount: DataTypes.INTEGER,
    likeCount: DataTypes.INTEGER,
    commentCount: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Resources',
  });
  return Resources;
};
