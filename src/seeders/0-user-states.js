'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('User_states', [{
            role: "Banned"
        },{
            role: "Visitor"
        }, {
            role: "Verified"
        }, {
            role: "Moderator"
        }, {
            role: "Admin"
        }, {
            role: "SuperAdmin"
        }], {});
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
    },

    down: async (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('User_states', null, {});
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
