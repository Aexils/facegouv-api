'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const user_state = await queryInterface.sequelize.query(
      `SELECT id from User_states where id = 5;`
    );

    const superAdmin = user_state[0];
    return queryInterface.bulkInsert('Users', [{
      UserStateId: superAdmin[0].id,
      firstName: "John",
      lastName: "Doe",
      age: "30",
      email: "john.doe@gmail.com",
      //A..y.6
      password: "$2b$05$8.t6EHeUfkZlmAQJ0/MtYuxiMhJiRwhJ5V8wRX.d0n2IebLvUavry",
      tel: "06.12.34.56.78",
      postalCode: "76000",
      isEnabled: 1,
      profilePicture: 'shiba.jpg'
    }], {});
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
