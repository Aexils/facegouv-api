'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Resources', [{
      UserId: 1,
      ResourceCategoryId: 1,
      ResourceStateId: 2,
      title: 'Ressource test',
      description: 'Description de la ressource test',
      eventDate: null,
      eventPlace: null,
      viewCount: 0,
      searchCount: 0,
      likeCount: 0,
      commentCount: 0,
      createdAt: Date.now(),
      updatedAt: Date.now()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
