import SecurityToken from '../api/SecurityTokenAPI';
import Auth from '../api/AuthAPI';
import Resource from '../api/ResourceAPI';
import Comment from '../api/CommentAPI';
import {Router} from 'express'
import Conversation from '../api/ConversationAPI';
import Relationships from '../api/RelationshipAPI';
import FavoriteAPI from "../api/FavoriteAPI";
import Upload from '../middlewares/upload';

const router = new Router();

//region user
router.route('/users/register')
  .post(async (req, res) => {
    try {
      return await new Auth().Register(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/users/activation')
  .post(async (req, res) => {
    try {
      return await new Auth().Activation(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/users/login')
  .post(async (req, res) => {
    try {
      return await new Auth().Login(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/users/protected')
  .get((req, res, next) => {
    try {
      return new SecurityToken().EnsureToken(req, res, next);
    } catch (error) {
      return error;
    }
  })

router.route('/users/forgotten_password')
  .post((req, res) => {
    try {
      return new Auth().ForgottenPassword(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/users/edit_account')
  .put(Upload.upload.fields([{name: 'image'}, {name: 'user'}]), (req, res) => {
    try {
      return new Auth().UpdateAccount(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/users/edit_password')
  .put((req, res) => {
    try {
      return new Auth().UpdatePassword(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/user/:id')
  .get((req, res) => {
    try {
      return new Auth().GetUserById(req, res);
    } catch (error) {
      return error;
    }
  })

//endregion user

//region resource
router.route('/resource')
  .get(async (req, res) => {
    try {
      console.log(req)
      await new Resource().GetAllResources(req, res);
    } catch (error) {
      error;
    }
  })
  .post(Upload.upload.fields([{name : 'image'}, {name: 'resource'}]), async (req, res) => {
    try {
      return await new Resource().CreateResource(req, res);
    } catch (error) {
      return error;
    }
  })
  .put(async (req, res) => {
    try {
      await new Resource().UpdateResourceStateIdFromAdmin(req, res);
    } catch (error) {
      error;
    }
  })

router.route('/resource_not_valided')
  .get(async (req, res) => {
    try {
      await new Resource().GetAllResourcesNotValided(req, res);
    } catch (error) {
      error;
    }
  })

router.route('/resources/:number')
  .get(async (req, res) => {
    try {
      return await new Resource().GetResourcesByNumberAndDateOrder(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/resourcesByUser/:userId')
  .get(async (req, res) => {
    try {
      return await new Resource().GetResourcesByUserId(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/resource/:id')
  .get(async (req, res) => {
    try {
      return await new Resource().GetResourceById(req, res)
    } catch (error) {
      return error;
    }
  })
  .put(async (req, res) => {
    try {
      await new Resource().UpdateResource(req, res);
    } catch (error) {
      return error;
    }
  })
  .delete(async (req, res) => {
    try {
      await new Resource().DeleteResource(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/resource/like')
  .post(async (req, res) => {
    try {
      return await new Resource().LikeResource(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/resource/view')
  .post(async (req, res) => {
    try {
      return await new Resource().IncrementResourceView(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/resources/shared/:userId')
  .get(async (req, res) => {
    try {
      return await new Resource().GetResourcesShared(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion resource

//region comment
router.route('/comment/:resourceId/:commentId')
  .get(async (req, res) => {
    try {
      await new Comment().GetCommentById(req, res);
    } catch (error) {
      return error;
    }
  })
  .put(async (req, res) => {
    try {
      await new Comment().UpdateComment(req, res)
    } catch (error) {
      return error;
    }
  })

router.route('/comment')
  .post(async (req, res) => {
    try {
      return await new Comment().AddComment(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/comments/:resourceId')
  .get(async (req, res) => {
    try {
      return await new Comment().GetAllCommentsByResourceId(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion comment

//region subComment
router.route('/subComment')
  .post(async (req, res) => {
    try {
      return await new Comment().AddSubComment(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/subComment/:commentId')
  .get(async (req, res) => {
    try {
      return await new Comment().GetAllSubCommentsByCommentId(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/subComment/:subCommentId')
  .delete(async (req, res) => {
    try {
      return await new Comment().DeleteSubComment(req, res);
    } catch (error) {
      return error;
    }
  })

router.route('/subComment/:commentId/:subCommentId')
  .get(async (req, res) => {
    try {
      return await new Comment().GetSubCommentById(req, res);
    } catch (error) {
      return error;
    }
  })
  .put(async (req, res) => {
    try {
      return await new Comment().UpdateSubComment(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion subComment

//region conversations
router.route('/conversations/:userId')
  .get(async (req, res) => {
    try {
      return await new Conversation().GetConversations(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/conversation/:convId')
  .get(async (req, res) => {
    try {
      return await new Conversation().GetConversation(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion conversations

//region relationships
router.route('/relationships/:userId')
  .get(async (req, res) => {
    try {
      return await new Relationships().GetRelations(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion relationships

//region favorite
router.route('/favorites/:userId')
  .get(async (req, res) => {
    try {
      return await new FavoriteAPI().GetAllFavorites(req, res)
    } catch (error) {
      return error;
    }
  })
router.route('/favorite')
  .post(async (req, res) => {
    try {
      return await new FavoriteAPI().CreateFavorite(req, res);
    } catch (error) {
      return error;
    }
  })
router.route('/favorite/:id')
  .delete(async (req, res) => {
    try {
      return await new FavoriteAPI().RemoveFavorite(req, res);
    } catch (error) {
      return error;
    }
  })
//endregion favorite

export default router;
