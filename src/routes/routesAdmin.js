import Admin from '../api/AdminAPI';
import {Router} from 'express'

const router = new Router();

router.route('/admin/all_users')
    .get(async(req, res) => {
        try {
            return await new Admin().GetAllUsers(req, res);
        } catch (error) {
            return error;
        }
    })

router.route('/admin/all_users_except_admin')
    .get(async(req, res) => {
        try {
            return await new Admin().GetAllUsersExceptAdmin(req, res);
        } catch (error) {
            return error;
        }
    })

router.route('/admin/edit_user_state')
    .put(async(req, res) => {
        try {
            return await new Admin().EditUserState(req, res);
        } catch (error) {
            return error;
        }
    })

export default router;
