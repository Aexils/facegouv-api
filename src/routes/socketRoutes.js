import Relationships from '../api/RelationshipAPI';
import Conversation from '../api/ConversationAPI';
import Resource from "../api/ResourceAPI";

let sessionsMap = [];

export async function socketRouter(io, socket) {
    // Ask userId to link with socketId
    socket.emit('askForUserId');
    // Link userId with socketId
    socket.on('userIdReceived', (userId) => {
        sessionsMap.push({userId, socketId: socket.id});
    });
    // Unlink userId and socketId
    socket.on('disconnect', () => {
        sessionsMap = sessionsMap.filter((object) => object.socketId !== socket.id);
        console.log('User disconnected');
    });

    // Conversations
    socket.on('createConv', async function(data) {
        await new Conversation().CreateConversation(io, socket, sessionsMap, data);
    });

    // Conversations
    socket.on('renameConv', async function(data) {
        await new Conversation().RenameConv(io, socket, sessionsMap, data);
    });

    socket.on('deleteConv', async function(data) {
        await new Conversation().DeleteConversation(io, socket, sessionsMap, data);
    });

    socket.on('addMessage', async function(data) {
        await new Conversation().AddMessage(io, socket, sessionsMap, data);
    });

    socket.on('removeMessage', async function(data) {
        await new Conversation().RemoveMessage(io, socket, sessionsMap, data);
    });
    

    socket.on('addUserToConv', async function(data) {
        await new Conversation().AddToConversation(io, socket, sessionsMap, data);
    });

    socket.on('getUserConversations', async function(data) {
        await new Conversation().GetConversations(io, socket, sessionsMap, data);
    });

    socket.on('leaveConv', async function(data) {
        await new Conversation().LeaveConversation(io, socket, sessionsMap, data);
    });

    socket.on('readMessages', async function(data) {
        await new Conversation().ReadMessages(io, socket, sessionsMap, data);
    })

    socket.on('kickUserFromConv', async function(data) {
        await new Conversation().KickFromConversation(io, socket, sessionsMap, data);
    });

    // Relationships
    socket.on('addFriend', async function(data) {
        await new Relationships().AddFriend(io, socket, sessionsMap, data);
    });

    socket.on('confirmFriend', async function(data) {
        await new Relationships().ConfirmFriend(io, socket, sessionsMap, data);
    });

    socket.on('removeFriend', async function(data) {
        await new Relationships().RemoveFriend(io, socket, sessionsMap, data);
    });

    socket.on('getRelations', async function(data) {
        await new Relationships().GetRelations(io, socket, sessionsMap,  data);
    });

    socket.on('getRelationsToConfirm', async function(data) {
        await new Relationships().GetRelationsToConfirm(io, socket, sessionsMap, data);
    });

    socket.on('shareResource', async function(data) {
        await new Resource().ShareResourceToFriends(io, socket, sessionsMap, data);
    });
}
