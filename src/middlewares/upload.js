import multer from 'multer';
import path from 'path';
export default class Upload {
    static storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public')
        },
        filename: function (req, file, cb) {
            const unique = Date.now() + '-' + Math.round(Math.random() * 1E9)
            cb(null, unique + path.extname(file.originalname))
        }

    })

    static upload = multer({ storage: Upload.storage});
}
