const jwt = require("jsonwebtoken");

export default class AuthCheck {
    checkToken(req, res, next) {
        try {
            const token = req.header("x-auth-token");
            if (!token) return res.status(403).send("Access denied.");
    
            const decoded = jwt.verify(token, 'my_secret_key');
            req.user = decoded;

            if (req.user.id) {
                
            }

            next();
        } catch (error) {
            res.status(400).send("Invalid token");
        }
    }
};