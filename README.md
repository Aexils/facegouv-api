# FaceGouv-API

API des applications FaceGouv™

****

### **Démarrer l'application**

Lancer le serveur en mode développement
`npm run dev`

### **Tests unitaires**

Vider et créer la base de données de test
`npm run db:test`

Lancer le serveur en mode test
`npm run test`

Pour voir les emails (optionnel) `npm run maildev`

Lancer les tests `npm run mocha`
