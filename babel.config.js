'use strict';


module.exports = {
	presets: [
		['@babel/env', {
			targets: { node: '10' },
			shippedProposals: true
		}]
	],

	plugins: [
		["module-resolver", { "root": ["./src"] }],
		'@babel/plugin-syntax-dynamic-import',
		'dynamic-import-node',
		'@babel/plugin-proposal-class-properties'
	],
};
