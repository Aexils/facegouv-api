FROM node:14
# Create app directory
WORKDIR /app
# modules
COPY package.json ./
RUN npm install
# build
COPY . .
RUN npm run build
EXPOSE 3000
# run
CMD npm run start
