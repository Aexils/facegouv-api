'use strict';

const gulp        = require("gulp");
const babel       = require("gulp-babel");
const sourcemaps  = require('gulp-sourcemaps');
const plumber     = require('gulp-plumber');
const del         = require("del");


const config = {
	src: 'src',
	dist: 'dist',
};


gulp.task("clean-dist", function() {
	return del([`${config.dist}/**`,`!${config.dist}`]);
});

gulp.task("build-js", ['clean-dist'], function() {
	return gulp.src(`${config.src}/**/*.js`)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(babel())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(config.dist));
});

gulp.task("build-assets", ['clean-dist'], function() {
	return gulp.src([
			`${config.src}/**`,
			`!${config.src}/**/*.js`,
		])
		.pipe(plumber())
		.pipe(gulp.dest(config.dist));
});


gulp.task('clean', ['clean-dist']);
gulp.task('build', ['build-js','build-assets']);
