import {expect} from 'chai';
var request = require('supertest');

describe('Resources', () => {
    var server;
    beforeEach(function () {
        server = require('./server');
    });
    afterEach(function () {
        server.close();
    });
    it('It should GET all the resources', function testSlash(done) {
        request(server)
            .get('/resource')
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.be.equal(1);
            })
            .end(done)
    });
    it('It should GET a resource by id', function testSlash(done) {
        request(server)
            .get('/resource/1')
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('object');
                expect(res.body.title).to.be.equal('Ressource test');
            })
            .end(done)
    });
    it('It should GET a number of resources filtered by date', function testSlash(done) {
        request(server)
            .get('/resources/1')
            .expect(200)
            .expect(res => {
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.be.equal(1);
            })
            .end(done)
    });
    it('It should POST a resource', function testSlash(done) {
        const resource = {userId: 1, category: 'Publication', title: 'Ressource test 2', description: 'Description de la ressource créée pendant les tests avec Mocha'};
        request(server)
            .post('/resource')
            .field('resource', '{"userId":1,"resource":{"title":"Ressource test 2","description":"Description de la ressource créée pendant les tests avec Mocha","category":"Publication"}}')
            .expect(201)
            .expect(res => {
                expect(res.body.Message).to.be.equal('Ressource créée avec succès !');
            })
            .end(done)
    });
    it('It should GET all resources not valided', function testSlash(done) {
        request(server)
            .get('/resource_not_valided')
            .expect(200)
            .expect((res) => {
                expect(res.body.data).to.be.a('number');
            })
            .end(done)
    });
    it('It should PUT the resourceStateId of a resource', function testSlash(done) {
        const resource = {id: 2, resourceStateId: 2, stateAdmin: 6};
        request(server)
            .put('/resource')
            .send(resource)
            .expect(200)
            .expect(res => {
                expect(res.body.message).to.be.equal('Ressource mis à jour avec succès !');
            })
            .end(done)
    });
    // TODO A revoir
    /*it('It should DELETE the resource', function testSlash(done) {
        const resource = {id: 2, resourceStateId: 2, stateAdmin: 6};
        request(server)
            .del('/resource')
            .send(resource)
            .expect(200)
            .expect(res => {
                expect(res.body.message).to.be.equal('Ressource mis à jour avec succès !');
            })
            .end(done)
    });*/
});
