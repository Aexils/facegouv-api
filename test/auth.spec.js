import {expect} from 'chai';
var request = require('supertest');

describe('Auth', () => {
    var server;
    beforeEach(function () {
        server = require('./server');
    });
    afterEach(function () {
        server.close();
    });
    it('It should POST a new user', function testSlash(done) {
        const user = {UserStateId: 5, firstName: "Jean", lastName: "Test", age: "30", email: "user-test@test.fr", password: "Motdepasse1", tel: "06.12.34.56.78", postalCode: "76000", isEnabled: 1}
        request(server)
            .post('/users/register')
            .send(user)
            .expect(201)
            .expect((res) => {
                expect(res.body.Message).to.be.equal('Utilisateur crée avec succès !');
            })
            .end(done)
    });
    it('It should POST a login', function testSlash(done) {
        const user = {email: 'user-test@test.fr', password: 'Motdepasse1'}
        request(server)
            .post('/users/login')
            .send(user)
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('string');
            })
            .end(done)
    });
    it('It should PUT a user', function testSlash(done) {
        const user = {id: 2, firstName: "Test", lastName: "Jean", age: "30", email: "user-test@test.fr", tel: "06.12.34.56.78", postalCode: "76000", isEnabled: 1, profilePicture: "shiba.jpg"};
        request(server)
            .put('/users/edit_account')
            .field('user', '{"id": 2, "firstName": "Test", "lastName": "Jean", "age": "30", "email": "user-test@test.fr", "tel": "06.12.34.56.78", "postalCode": "76000", "isEnabled": 1, "profilePicture": "shiba.jpg"}')
            .expect(200)
            .expect(res => {
                expect(res.body.message).to.be.equal('Profil mis à jour avec succès !');
            })
            .end(done)
    });
    it('It should PUT a password user', function testSlash(done) {
        const user = {id: 2, password: 'Motdepasse1', newPassword: 'Motdepasse2'};
        request(server)
            .put('/users/edit_password')
            .send(user)
            .expect(200)
            .expect(res => {
                expect(res.body.message).to.be.equal('Mot de passe mis à jour avec succès !');
            })
            .end(done)
    });
    it('It should POST a new password', function testSlash(done) {
        const user = {email: 'user-test@test.fr'}
        request(server)
            .post('/users/forgotten_password')
            .send(user)
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('object');
                expect(res.body.message).to.be.equal('Mot de passe modifié avec succès');
            })
            .end(done)
    });
});
