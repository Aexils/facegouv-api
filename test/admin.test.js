import {expect} from 'chai';
var request = require('supertest');

describe('Admin', () => {
    var server;
    beforeEach(function () {
        server = require('./server');
    });
    afterEach(function () {
        server.close();
    });
    it('It should GET all users', function testSlash(done) {
        request(server)
            .get('/admin/all_users')
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('array');
            })
            .end(done)
    });
    it('It should GET all users except admin', function testSlash(done) {
        request(server)
            .get('/admin/all_users_except_admin')
            .expect(200)
            .expect((res) => {
                expect(res.body).to.be.a('array');
            })
            .end(done)
    });
    it('It should PUT a userStateId', function testSlash(done) {
        const user = {id: 1, userStateId: 4, stateAdmin: 5};
        request(server)
            .put('/admin/edit_user_state')
            .send(user)
            .expect(200)
            .expect(res => {
                expect(res.body.message).to.be.equal('Utilisateur mis à jour avec succès !');
            })
            .end(done)
    });
});
