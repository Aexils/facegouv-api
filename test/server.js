import cors from "cors";
import bodyParser from "body-parser";
import router from "../src/routes/routes";
import routerAdmin from "../src/routes/routesAdmin";

const express = require('express');

let app = express();
// Activer le CORS pour toutes les routes
app.use(cors());

// Activation du moteur de template
app.set('view engine', 'ejs');

//Configuration de Body Parser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

app.get('/', function (req, res) {
    res.status(200).send('ok');
});
var server = app.listen(3000, function () {
    var port = server.address().port;
    console.log('Test server listening at port %s', port);
});

// router
app.use(router);
app.use(routerAdmin);

module.exports = server;
